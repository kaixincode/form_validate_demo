﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ValidateDemo
{
    /// <summary>
    /// check 的摘要说明
    /// </summary>
    public class check : IHttpHandler
    {
        public HttpResponse Response { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Response = context.Response;
            switch (context.Request["action"])
            {
                case "reg":
                    Reg();
                    break;
                default:
                    if (context.Request["username"] == "admin")
                    {
                        context.Response.Write("false");
                    }
                    else
                    {
                        context.Response.Write("true");
                    }
                    break;
            }

        }
        private void Reg()
        {
            HttpContext.Current.Response.Write("ok");
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}